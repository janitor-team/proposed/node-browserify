#!/bin/sh
BROWSERIFY=${BROWSERIFY:-browserify}

${BROWSERIFY} --debug -e ./js/main.js > js/build/bundle.js

echo bundle was generated with source maps, you can now open index.html
