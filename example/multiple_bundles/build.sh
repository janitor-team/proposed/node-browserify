#!/bin/sh
BROWSERIFY=${BROWSERIFY:-browserify}
$BROWSERIFY -r ./robot.js > static/common.js
$BROWSERIFY -x ./robot.js beep.js > static/beep.js
$BROWSERIFY -x ./robot.js boop.js > static/boop.js
